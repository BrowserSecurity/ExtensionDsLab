namespace LogicalHacking.ExtensionDsLab.Tests

open Expecto
open FsCheck
open GeneratorsCode

open System
open System.Configuration

open LogicalHacking.ExtensionDsLab
open LogicalHacking.ExtensionDsLab.Archive.DbConnector
open LogicalHacking.ExtensionDsLab.Archive.ExtensionQueries


module Tests =
    let config10k = { FsCheckConfig.defaultConfig with maxTest = 10000}
    // bug somewhere:  registering arbitrary generators causes Expecto VS test adapter not to work
    //let config10k = { FsCheckConfig.defaultConfig with maxTest = 10000; arbitrary = [typeof<Generators>] }
    let configReplay = { FsCheckConfig.defaultConfig with maxTest = 10000 ; replay = Some <| (1940624926, 296296394) }

    let connectionStringOrDefault defaultString =
        let map = new ExeConfigurationFileMap(ExeConfigFilename = __SOURCE_DIRECTORY__ + "/app.config")
        let config = ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None)
        match config.ConnectionStrings.ConnectionStrings.["ProductionExtensionDB"] with
                            | null -> defaultString
                            | a -> a.ConnectionString

    let ctx = ExtensionDbProvider.GetDataContext(connectionStringOrDefault "")

    let measureExecutionTime f = 
        let timer = new System.Diagnostics.Stopwatch()
        timer.Start()
        let result = f()
        (int32(timer.ElapsedMilliseconds), result)


    [<Tests>]
    let testSimpleTests =
        testList "DomainTypes.Tag" [
            testCase "equality" <| fun () ->
                let result = 42
                Expect.isTrue (result = 42) "Expected True"

            testPropertyWithConfig config10k "whitespace" <|
                fun  () ->
                    Prop.forAll (Arb.fromGen <| whitespaceString())
                        (fun (x : string) -> 
                            x = x)
        
            testCase "Testing Performance of getDownloads" <| fun () ->
                let (time, result) = measureExecutionTime (fun () -> (getDownloads ctx "lkllajgbhondgjjnhmmgbjndmogapinp"))
                Expect.isLessThan time 1500 "Query should take less than 1500ms"
        ]
        
