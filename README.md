# ExtensionDsLab

ExtensionDsLab is a data science and machine learning framework for analyzing
security and privacy related properties of the browser  extensions (and Chrome
Apps) offered in the [Chrome Web Store](https://chrome.google.com/webstore/).

## Installation

### First Time

Ensure that both

* [mono](https://www.mono-project.com/) (tested with version 5.14) *and*
* [dotnet SDK](https://www.microsoft.com/net/download) (tested with version 2.1)

are installed. Moreover, [FAKE] needs to be installed

        dotnet tool install fake-cli -g

and added to the `PATH` so that you can call `fake`  on the command line.
This might require

        export PATH=$HOME/.dotnet/tools/fake:$PATH

After cloning the repository, call

        ./configure.sh  

to initialize the `paket` configuration.

### Database Configuration

Both the application itself and the tests require that the database connection
is configured properly. For this,

* copy `src/LogicalHacking.ExtensionDsLab/app.config.in`
  to `src/LogicalHacking.ExtensionDsLab/app.config  
* edit `src/LogicalHacking.ExtensionDsLab/app.config` and
  replace in the connection string the correct values for `<SERVER>`,
  `<LOGIN>`, and `<PWD`>.
* repeat the previous steps for `src/LogicalHacking.ExtensionDsLab.Tests/app.config.in` .

### (Re-) Compiling

After that, the project can be build using

        ./build.sh

### Note for Windows Users

On Windows (untested), `configure.sh` can be replaced by configuring
`paket` manually

        .paket/paket.bootstrapper.exe
        .paket/paket.exe install
  
After that, the project can be build using

        build.cmd

## Team

* [Achim D. Brucker](https://www.brucker.ch/)

## License

This project is licensed under the GPL 3.0 (or any later version).

SPDX-License-Identifier: GPL-3.0-or-later

## Master Repository

The master git repository for this project is hosted by the [Software
Assurance & Security Research Team](https://logicalhacking.com) at
<https://git.logicalhacking.com/BrowserSecurity/ExtensionDsLab>.
