(*
 * This file is part of the ExtensionDsLab project.
 * Copyright (c) 2017 LogicalHacking.com 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *)
 
namespace LogicalHacking.ExtensionDsLab.Archive

open FSharp.Data.Sql
open System.IO

module DbConnector = 
    [<Literal>] 
    let ConnectionStringName = @"ProductionExtensionDB"
    [<Literal>] 
    let ResolutionPath = __SOURCE_DIRECTORY__ + @"/../../packages/MySql.Data/lib/net452"

    // create a type alias with the connection string and database vendor settings
    type ExtensionDbProvider = SqlDataProvider<
                                 DatabaseVendor = Common.DatabaseProviderTypes.MYSQL,
                                 ConnectionStringName = ConnectionStringName,
                                 ResolutionPath = ResolutionPath,
                                 IndividualsAmount = 500,
                                 UseOptionTypes = true >

    type ExtensionDbType = ExtensionDbProvider.dataContext
