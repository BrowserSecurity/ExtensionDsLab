(*
 * This file is part of the ExtensionDsLab project.
 * Copyright (c) 2017 LogicalHacking.com 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *)
 
namespace LogicalHacking.ExtensionDsLab.Archive

/// Queries realted to extensions. The return type of all queries
/// in this module should be `System.Linq.IQueryable<>` .
module ExtensionQueries = 
    open DbConnector
    
    /// **Description**
    /// 
    /// Query returning historical download data (as reported by Google) for 
    /// for the extension specified by the extension id `extid` .
    ///
    /// **Runtime Expectation**
    ///  
    ///  *Fast*, this query is expected to take less than 2s.
    /// 
    /// **Parameters**
    /// 
    ///   * `ctx` - current database context (type: `ExtensionDbType`)
    ///   * `extid` - id of the extension for which the download data
    ///               is obtained (type: `string`)
    ///
    /// **Output Type**
    /// 
    ///   * `System.Linq.IQueryable<System.DateTime * int>`
    ///
    /// **Exceptions**
    ///
    ///    * `MySql.Data.MySqlClient.MySqlException`
    ///
    let getDownloads (ctx:ExtensionDbType) extid  = 
        query {
                  for order in ctx.Extensions.Extension do 
                    where (order.Extid = extid && order.Downloads.IsSome)
                    select (order.Date, order.Downloads.Value)       
               } 
                
    /// **Description**
    /// 
    /// Query for obtaining a list of all extension id stored in the database.
    /// 
    /// **Runtime Expectation** 
    /// 
    /// *Slow*, this query can take several hours.
    /// 
    /// **Parameters**
    /// 
    ///   * `ctx` - current database context (type: `ExtensionDbType`)
    ///
    /// **Output Type**
    ///   * `System.Linq.IQueryable<string>`
    ///
    /// **Exceptions**
    ///
    ///    * `MySql.Data.MySqlClient.MySqlException`
    ///
    let getAllExtIds (ctx:ExtensionDbType)        =  
         query {
                  for ext in ctx.Extensions.Extension do
                    select ext.Extid
                    distinct
                }
