(*
 * This file is part of the ExtensionDsLab project.
 * Copyright (c) 2017, 2018 LogicalHacking.com 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *)

#load "ExtensionDsLab.fsx"
#load "Themes/DefaultWhite.fsx"

open LogicalHacking.ExtensionDsLab
open LogicalHacking.ExtensionDsLab.Archive.DbConnector
open LogicalHacking.ExtensionDsLab.Archive.ExtensionQueries

open Deedle
open FSharp.Data
open XPlot.GoogleCharts
open XPlot.GoogleCharts.Deedle

// the connection string shall be of the form 
// "Server=<DBHOST>;Database=extensions;User=<USER>;Password=<PASSWORD>"
let ctx = ExtensionDbProvider.GetDataContext(connectionStringOrDefault "",commandTimeout=36000)

// Log SQL: https://fsprojects.github.io/SQLProvider/core/querying.html
FSharp.Data.Sql.Common.QueryEvents.SqlQueryEvent |> Event.add (printfn "Executing SQL: %O")


let chartDownloads extIds = (List.map (fun e -> (series (getDownloads ctx e))) extIds)
                            |> Chart.Line
                            |> Chart.WithOptions (Options(legend=Legend(position="bottom"), width=1100))
                            |> Chart.WithLabels extIds

chartDownloads ["lkllajgbhondgjjnhmmgbjndmogapinp"]
// chartDownloads (List.take 5 (getAllExtIds ctx |> Seq.toList))

let extData = 
    query {for x in ctx.Extensions.Extension do
             select (x.Date, x.Name, x.Downloads) }
    |> Seq.filter (fun (date, name, download) -> name.IsSome && download.IsSome)
    |> Seq.map (fun (date, name, download) -> (date, name.Value, download.Value))    
    |> Frame.ofRecords
    |> Frame.indexColsWith ["Date"; "Name"; "Downloads"]

