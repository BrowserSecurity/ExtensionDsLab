(*
 * This file is part of the ExtensionDsLab project.
 * Copyright (c) 2018 LogicalHacking.com 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *)

namespace LogicalHacking.ExtensionDsLab

#I "../../packages/FSharp.Data/lib/net40"
#I "../../packages/RProvider/lib/net40"

#r "System.Configuration.dll"
open System.Configuration

#I "../../bin/LogicalHacking.ExtensionDsLab/net47"
#r "LogicalHacking.ExtensionDsLab.dll"

#I @"../../packages/SQLProvider/lib/net451"
#r @"FSharp.Data.SqlProvider.dll"

#I "../../packages/FSharp.Data/lib/net40"
#r "FSharp.Data.dll"

#load "../../packages/FsLab/FsLab.fsx"

[<AutoOpen>]
module ScriptConfig=
    open LogicalHacking.ExtensionDsLab.Archive.DbConnector

    let connectionStringOrDefault ctx = 
        let map = new ExeConfigurationFileMap(ExeConfigFilename = __SOURCE_DIRECTORY__ + "/app.config")
        let config = ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None)
        match config.ConnectionStrings.ConnectionStrings.["ProductionExtensionDB"] with
                            | null -> ctx
                            | a -> a.ConnectionString
